import { createUUID } from "./utils.js";


const STORAGE_KEY = "note-e";

const getId = (item) => {
    let nid = item.id;
    if (nid === undefined) {
        nid = createUUID();
    }
    return nid;
};

const retrieveStorage = () => {
    let content = window.localStorage.getItem(STORAGE_KEY);
    if (content === null || content === "") {
        content = "[]";
    }
    return JSON.parse(content);

};

export const getNotes = () => {
    let content = retrieveStorage();
    const notes = [];
    for (const item of content) {
        notes.push({
            id: getId(item),
            body: (item.body || '')
        });
    }
    return notes;
};

export const getNote = (uuid) => {
    const content = getNotes();
    for (const note of content) {
        if (note.id === uuid) {
            return note;
        }
    }
    return null;
};


export const addNote = (body) => {
    const current = getNotes();
    const newNote = {
        id: createUUID(),
        body: body,
    };
    current.unshift(newNote);
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(current));
    return newNote;
};

export const updateNote = (updatedNote) => {
    const current = getNotes();
    for (let index = 0; index < current.length; index++) {
        if (current[index].id === updatedNote.id) {
            if (index !== 0) {
                // updated note becomes the first
                const prev = current[0];
                current[0] = updatedNote;
                current[index] = prev;
            } else {
                current[0] = updatedNote;
            }
            break;
        }
    }
    return window.localStorage.setItem(STORAGE_KEY, JSON.stringify(current));
};

export const removeNote = (note) => {
    const current = getNotes();
    for (let index = 0; index < current.length; index++) {
        if (current[index].id === note.id) {
            current.splice(index, 1);
            break;
        }
    }
    return window.localStorage.setItem(STORAGE_KEY, JSON.stringify(current));
};
