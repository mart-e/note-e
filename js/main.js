import * as database from "./database.js";
import * as ui from "./ui.js";


const makeid = (length) => {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
};


export const main = () => {


    let notes = database.getNotes();

    ui.render(notes);

    ui.addActionEvents();
};

main();
