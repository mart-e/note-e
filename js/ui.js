import * as database from "./database.js";

const onFocusOut = (event) => {
    const $article = event.target.parentNode;
    const wasUpdated = onBodyUpdateEvent(
        $article.getAttribute("data-id"),
        event.target.innerText
    );
    if (wasUpdated) {
        // move first position
        $article.parentNode.prepend($article);
    }
};

const onClickTrash = (event) => {
    const $article = event.target.parentNode;
    const note = database.getNote($article.getAttribute("data-id"));
    if (note) {
        database.removeNote(note);
    }
    $article.parentNode.removeChild($article);
};

const onClickAddNote = (event) => {
    const $main = document.getElementById("main");
    const $article = generateArticle({id: null, body: "New note"});
    $main.prepend($article);
};


const onBodyUpdateEvent = (uuid, body) => {
    const note = database.getNote(uuid);
    if (!note) {
        database.addNote(body);
    } else {
        if (note.body !== body) {
            note.body = body;
            database.updateNote(note);
            return true;
        }
    }
    return false;
};


const generateArticle = (note) => {
    const $article = document.createElement("article");
    $article.setAttribute("data-id", note.id || '');

    const $icon = document.createElement("img");
    $icon.setAttribute("src", "/icons/trash.svg");
    $icon.setAttribute("class", "icon-trash");
    $icon.addEventListener('click', onClickTrash);

    const $content = document.createElement("div");
    $content.setAttribute("class", "content");
    $content.contentEditable = true;
    $content.innerText = note.body;
    $content.addEventListener('focusout', onFocusOut);
    
    $article.append($icon);
    $article.append($content);

    return $article;
};

export const render = (notes) => {

    const $main = document.getElementById("main");
    $main.textContent = "";
    for (const note of notes) {
        const $article = generateArticle(note);
        $main.append($article);
    }

};

export const addActionEvents = () => {

    const $add = document.getElementById("action_add");
    $add.addEventListener('click', onClickAddNote);
};